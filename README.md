# MAPAS CONCEPTUALES

## MAPA 1: Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)

```plantuml
@startmindmap
*[#Orange] Programando ordenadores en los 80 y ahora
**[#lightgreen] Sistemas Antiguos
***_ ¿Que es?
****[#lightblue]:Es cualquier sistema que ya no esta a la venta 
o que no tuvo continuidad, por lo general son 
ordenadores de los años 80's y 90's.;
*****[#FFF071]:La velocidad de la CPU 
se media en MHz;
*****[#FFF071]:Para poder programar en las diferentes
maquinas debias de conocer la 
arquitectura de cada maquina;
*****[#FFF071] Los recursos eran limitados
*****[#FFF071]:Se programaba en lenguajes
de bajo nivel;
******[#E6E6E6] Ensamblador
**[#lightgreen] Sistemas Actuales
***_ ¿Que es?
****[#lightblue]:Son ordenadores mas nuevos. Por lo general 
son los que tenemos en casa o el que puedes 
comprar en una tienda.;
*****[#FFF071]:La velocidad se mide 
en GHz;
*****[#FFF071]:Las maquinas son distintas
en todos los sentidos;
*****[#FFF071]:Se programaba en diversos
lenguajes de alto nivel;
******[#E6E6E6] Java
******[#E6E6E6] C
******[#E6E6E6] C++
**[#lightgreen] Leyes
***[#lightblue] Ley de Moore
****_ ¿De que trata?
*****[#FFF071]:Cada 18 meses se puede
generar una cpu
el doble de rapida.;
***[#lightblue] Ley de Wirth
****_ ¿De que trata?
*****[#FFF071]:El sofware se vuelve
el doble de lento 
compensando la Ley de Moore.;

@endmindmap
```
## MAPA 2: Historia de los algoritmos y de los lenguajes de programación (2010)

```plantuml
@startmindmap
*[#Orange]:Historia de los algoritmos 
y de los lenguajes de programación;
**[#lightgreen] Algoritmo
***_ Es
****[#lightblue]:Conjunto de instrucciones 
ordenadas y finitas;
*****_ Se aplica en
******[#FFF071] Manuales de usuario
******[#FFF071] Ordenes que recibe un trabajador
*****_ Se divide en
******[#FFF071] Razonables
*******[#E6E6E6]:El tiempo de ejecucion crece
a medida que los problemas
aumentan;
******[#FFF071] No razonables
*******[#E6E6E6] Llamadas exponenciales
*******[#E6E6E6]:Llamadas Super
Polinominales;
**[#lightgreen] Lenguaje de programacion
***_ Es
****[#lightblue]:Se puede interpretar como
la representacion de un algoritmo;
*****_ Se divide en
******[#FFF071]:Primera generacion
del lenguaje;
*******[#E6E6E6] Codigo maquina
******[#FFF071]:Segunda generacion
del lenguaje;
*******[#E6E6E6] Lenguaje ensamblador
******[#FFF071]:Tercera generacion
del lenguaje;
*******[#E6E6E6] FORTRAN
*******[#E6E6E6] COBOL
*******[#E6E6E6] PL/I
*******[#E6E6E6] BASIC
******[#FFF071]:Cuarta generacion
del lenguaje;
*******[#E6E6E6] C
*******[#E6E6E6] C++
*******[#E6E6E6] HTML
*******[#E6E6E6] PYTHON
*******[#E6E6E6] JAVA
*******[#E6E6E6] JS
*******[#E6E6E6] PHP
@endmindmap
```
## MAPA 3: Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startmindmap
*[#Orange]:La evolución de los lenguajes 
y paradigmas de programación;
**[#lightgreen] Paradigma
***_ ¿Que es?
****[#lightblue] Forma de pensar
***_ Comenzo por
****[#lightblue] Lenguaje ensamblador
***[#lightblue]:Tipos de 
paradigmas;
****[#FFF071] Distribuido
*****[#E99FFF]:Existe comunicacion 
entre computadoras;
*****_ Lenguajes
******[#E6E6E6] Ada
******[#E6E6E6] E
******[#E6E6E6] Limbo
******[#E6E6E6] Oz
****[#FFF071] Funcional
*****[#E99FFF]:Utiliza funciones
matematicas y recursividad;
*****_ Lenguajes
******[#E6E6E6] Haskell
******[#E6E6E6] F
******[#E6E6E6] Eriang
****[#FFF071] Lógico
*****[#E99FFF] Retorna true o false
*****_ Lenguajes
******[#E6E6E6] Prolog
******[#E6E6E6] Mercury
******[#E6E6E6] ALF
****[#FFF071]:Programacion
concurrente;
*****[#E99FFF]:Existe paralelismo 
entre tareas;
*****_ Lenguajes
******[#E6E6E6] Ada
******[#E6E6E6] Java
****[#FFF071]:Programacion
orientada a objetos;
*****[#E99FFF]:Consiste en abstracciones
del mundo real;
*****_ Lenguajes
******[#E6E6E6] Java
******[#E6E6E6] Php
******[#E6E6E6] Js
****[#FFF071]:Programacion orientada
en aspectos;
*****[#E99FFF]:Agrega capas independientes.
Requiere una capa base dependiendo
la necesidad del programa;
****[#FFF071]:Programacion orientada
en componentes;
*****[#E99FFF]:Resuelve problemas, dialogos
cooperacion, coordinacion y negociacion;
@endmindmap
```